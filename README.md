# react-wallet-mejuri-test
This is the frontend side of a challenge about a small CR~~U~~D application in ReactJS.

Contains: 

* A single page with the Frontend part of a "Wallet", which lets you add credit cards.
* This is a single landing page, it does not have any user login.
* Every component except the Redux Actions and Reducers were tested. I was not able to make that in time.
* It has a pretty annoying but fair linter added for coding tidyness.
* I was not able to play around in the User Interface due to my limited time, I would have love to make some different suggestions but again, time was limited and sticking to the look and feel was a requirement.
* This is a standalone FE. There is another repo with the backend and a Credit Card validator mocked up.
* The sensitive data sent by the FE form to the BE is encrypted with a Public Key. The rest is not, since is somewhat less dangerous since the data needed for the user interaction is incomplete.

## Run the app

0. ```npm install```
0. ```npm start```

## Build the app
```npm run build```

This will build the app into the "dist" directory in the root of the project. It contains the index.html along with the minified assets, ready for production.

## Config

There is a small config file just for some parameters with the Backend in:

```config/api.js```

Just to connect to the backend and to put the public key for data encryption.

I hope we can discuss the ups and downs of the implementation.

Thanks!

- Marcos.
