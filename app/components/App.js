import React from 'react';
import Routes from 'routes';
import { I18n } from 'react-i18nify';
import { translations } from 'helpers/languageHelper';
import WalletPage from 'components/pages/wallet/Wallet';
import './App.scss';

class App extends React.Component {
  constructor(props) {
    super(props);

    // Setting up language
    I18n.setTranslations(translations);
    I18n.setLocale('en');
  }
  render() {
    return (
      <div className="wallet-mejuri-test">
        { Routes }
        <WalletPage />
      </div>
    );
  }
}

export default App;
