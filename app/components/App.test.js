import React from 'react';
import { shallow } from 'enzyme';
import WalletPage from 'components/pages/wallet/Wallet';
import App from './App';

function setup(props) {
  return shallow(<App {...props} />);
}

describe('App', () => {
  describe('WHEN rendered by default', () => {
    let wrapper;
    
    beforeAll(() => {
      wrapper = setup();
    });
    
    it('should have a container for the application', () => {
      expect(wrapper.find('.wallet-mejuri-test').length).toBe(1);
    });
    
    it('should contain a Wallet component, which has all the logic/behavior of the wallet functionality', () => {
      expect(wrapper.find(WalletPage).length).toBe(1);
    });
  });  
});