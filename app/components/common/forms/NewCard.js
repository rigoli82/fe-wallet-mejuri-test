import React from 'react';
import PropTypes from 'prop-types';
import { I18n } from 'react-i18nify';
import classnames from 'classnames';
import { isEmpty } from 'helpers/objectHelper';
import './NewCard.scss';

class Card extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      form: {
        number: '',
        cvv: '',
        expires: ''
      },
      errors: {},
      errorMessage: null
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const { name, value } = event.target;
    const { form, errors } = this.state;

    this.setState({
      form: { ...form, [name]: value },
      errors: { ...errors, [name]: false }
    });
  }

  validateForm() {
    const { form } = this.state;
    const errors = {};

    // Checking the 16-digit card number
    const numberRegex = /^[0-9]{16}$/;
    if (!form.number) {
      errors.number = I18n.t('forms.validation.emptyCardNumber');
    } else if (!numberRegex.test(form.number)) {
      errors.number = I18n.t('forms.validation.wrongCardNumber');
    }

    // Checking the 3-digit card CVV
    const cvvRegex = /^[0-9]{3}$/;
    if (!form.cvv) {
      errors.cvv = I18n.t('forms.validation.emptyCVV');
    } else if (!cvvRegex.test(form.cvv)) {
      errors.cvv = I18n.t('forms.validation.wrongCVV');
    }

    // Checking the expiration date (MM/YYYY)
    const expiresRegex = /^(0[1-9]|1[0-2])\/20\d\d$/;

    if (!form.expires) {
      errors.expires = I18n.t('forms.validation.emptyExpires');
    } else if (!expiresRegex.test(form.expires)) {
      errors.expires = I18n.t('forms.validation.wrongExpires');
    } else {
      // Let's check the month/year is not in the past
      const [expMonth, expYear] = form.expires.split('/');
      const d = new Date().toISOString().slice(0, 7).replace('-', '');
      const expd = `${expYear}${expMonth}`;
      if (expd < d) {
        errors.expires = I18n.t('forms.validation.alreadyExpired');
      }
    }

    this.setState({ errors });

    return isEmpty(errors);
  }

  handleSubmit(e) {
    e.preventDefault();
    const { onSubmit } = this.props;
    const { form } = this.state;

    if (this.validateForm()) {
      onSubmit(form);
    }
  }

  formErrors() {
    const { customErrors } = this.props;
    const { errors } = this.state;
    const allErrors = {
      ...errors,
      ...customErrors
    };

    let returnErrors;
    // If it doesn't have any errors, we don't draw anything.
    if (!isEmpty(allErrors)) {
      returnErrors = (
        <div className="form__errors">
          { Object.keys(allErrors).map((name, key) => (
            <div key={key} className="form__error">{allErrors[name]}</div>
          )) }
        </div>
      );
    }

    return returnErrors;
  }

  render() {
    const { disabled, onCancel } = this.props;
    const { form, errors } = this.state;

    const classNames = classnames('new-card-form', { 'new-card-form--disabled': disabled });

    return (
      <div className={classNames}>
        <form className="form" onSubmit={this.handleSubmit}>
          <div className="new-card-form__row">
            <div className="new-card-form__field">
              <label htmlFor="number" className="form__label">{ I18n.t('forms.newCard.cardNumber') }</label>
              <input
                type="text"
                id="number"
                name="number"
                placeholder="0000000000000000"
                className={ classnames('form__element', { 'form__element--error': errors.number }) }
                value={ form.number }
                onChange={ this.handleChange }
                maxLength={16}
              />
            </div>
          </div>
          <div className="new-card-form__row">
            <div className="new-card-form__field">
              <label htmlFor="cvv" className="form__label">{ I18n.t('forms.newCard.cvv') }</label>
              <input
                type="text"
                id="cvv"
                name="cvv"
                placeholder="000"
                className={ classnames('form__element', { 'form__element--error': errors.cvv }) }
                value={ form.cvv }
                onChange={ this.handleChange }
                maxLength={3}
              />
            </div>
            <div className="new-card-form__field">
              <label htmlFor="expires" className="form__label">{ I18n.t('forms.newCard.expirationDate') }</label>
              <input
                type="text"
                id="expires"
                name="expires"
                placeholder="mm/yyyy"
                className={ classnames('form__element', { 'form__element--error': errors.expires }) }
                value={ form.expires }
                onChange={ this.handleChange }
                maxLength={7}
              />
            </div>
          </div>
          <div className="new-card-form__actions">
            <button type="button" className="btn new-card-form__cancel" onClick={ onCancel } >{ I18n.t('forms.newCard.cancel') }</button>
            <button type="submit" className="btn btn--primary new-card-form__submit">{ I18n.t('forms.newCard.send') }</button>
          </div>
          { this.formErrors() }
        </form>
      </div>
    );
  }
}

Card.defaultProps = {
  disabled: false,
  customErrors: {}
};

Card.propTypes = {
  customErrors: PropTypes.object,
  disabled: PropTypes.bool,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func
};

export default Card;
