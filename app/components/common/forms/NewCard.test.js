import React from 'react';
import { shallow } from 'enzyme';
import NewCard from './NewCard';

function setup(props) {
  return shallow(<NewCard {...props} />);
}

describe('NewCard', () => {
  describe('WHEN rendered by default', () => {
    let wrapper, instance;
    
    beforeAll(() => {
      wrapper = setup();
      instance = wrapper.instance();
    });
    
    it('should render a component container with a form', () => {
      expect(wrapper.find('.new-card-form').length).toBe(1);
      expect(wrapper.find('.new-card-form form').length).toBe(1);
      expect(wrapper.find('.new-card-form form').props().onSubmit).toEqual(instance.handleSubmit);
    });
    
    it('Should have three fields, a number, a cvv and an expiration date', () => {
      const htmlForm = wrapper.find('.new-card-form form');
      const number = htmlForm.find('input[name="number"]');
      const cvv = htmlForm.find('input[name="cvv"]');
      const expires = htmlForm.find('input[name="expires"]');

      expect(number.length).toBe(1);
      expect(number.props()).toEqual({
        type: 'text',
        id: 'number',
        name: 'number',
        value: '',
        placeholder: '0000000000000000',
        className: 'form__element',
        onChange: instance.handleChange,
        maxLength: 16
      });

      expect(cvv.length).toBe(1);
      expect(cvv.props()).toEqual({
        type: 'text',
        id: 'cvv',
        name: 'cvv',
        value: '',
        placeholder: '000',
        className: 'form__element',
        onChange: instance.handleChange,
        maxLength: 3
      });

      expect(expires.length).toBe(1);
      expect(expires.props()).toEqual({
        type: 'text',
        id: 'expires',
        name: 'expires',
        value: '',
        placeholder: 'mm/yyyy',
        className: 'form__element',
        onChange: instance.handleChange,
        maxLength: 7
      });
    });

    it('Should have an actions section with two buttons, a send and a cancel', () => {
      const htmlActions = wrapper.find('.new-card-form__actions');
      expect(htmlActions.length).toBe(1);
      expect(htmlActions.find('button').length).toBe(2);
      
      const cancel = htmlActions.find('.new-card-form__cancel');
      const submit = htmlActions.find('.new-card-form__submit');

      expect(cancel.length).toBe(1);
      expect(cancel.props().type).toBe('button');
      expect(cancel.props().onClick).toBe(wrapper.props().onClick);

      expect(submit.length).toBe(1);
      expect(submit.props().type).toBe('submit');
    });
  });
})
