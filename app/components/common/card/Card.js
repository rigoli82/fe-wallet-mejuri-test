import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import VisaCard from 'assets/svg/VisaCard';
import './Card.scss';

class Card extends React.Component {
  render() {
    const { trailingNumber, expires, disabled, onRemove } = this.props;

    if (!trailingNumber) return;

    const padding = '*'.repeat(16 - trailingNumber.length);
    const fullNumber = `${padding}${trailingNumber}`;
    const chunks = fullNumber.match(/.{1,4}/g);
    const formattedNumber = chunks.join(' ');
    const formattedExpires = expires.replace('-', '/');
    const classNames = classnames('card', { 'card--disabled': disabled });

    return (
      <div className={classNames}>
        <div className="card__header"><a className="card__remove" onClick={onRemove}>&times;</a></div>
        <div className="card__body">
          <div className="card__icon"><VisaCard /></div>
          <div className="card__number">{formattedNumber}</div>
          <div className="card__expires">{formattedExpires}</div>
        </div>
      </div>
    );
  }
}

Card.defaultProps = {
  disabled: false
};

Card.propTypes = {
  trailingNumber: PropTypes.string,
  expires: PropTypes.string,
  disabled: PropTypes.bool,
  onRemove: PropTypes.func
};

export default Card;
