import React from 'react';
import { shallow } from 'enzyme';
import Card from './Card';

function setup(props) {
  return shallow(<Card {...props} />);
}

describe('Card', () => {
  describe('WHEN rendered by default', () => {
    let wrapper;
    
    const props = {
      trailingNumber: '1234',
      expires: '04/2018'
    };
    
    beforeAll(() => {
      wrapper = setup(props);
    });
    
    it('should render a header and a body', () => {
      expect(wrapper.find('.card__header').length).toBe(1);
      expect(wrapper.find('.card__body').length).toBe(1);
    });
    
    it('should have an icon, a number and an expiration date in the body', () => {
      const body = wrapper.find('.card__body');
      
      // Checking the idon and the SVG component
      expect(body.find('.card__icon').length).toBe(1);
      expect(body.find('.card__icon VisaCard').length).toBe(1);
      
      // Checking the card number and the text
      expect(body.find('.card__number').length).toBe(1);
      expect(body.find('.card__number').text()).toBe('**** **** **** 1234');
      
      // Checking the expiration date and how it is formatted
      expect(body.find('.card__expires').length).toBe(1);
      expect(body.find('.card__expires').text()).toBe('04/2018');
    });
    
    it('should have a remove element on the header with the onClick attached to it', () => {
      const remove = wrapper.find('.card__header .card__remove');
      expect(remove.length).toBe(1);
      expect(remove.props().onClick).toEqual(props.onClick);
    });
  });
  
  describe('WHEN passing on the disabled flag', () => {
    let wrapper;
    const props = {
      trailingNumber: '1234',
      expires: '04/2018',
      disabled: true
    };
    
    beforeAll(() => {
      wrapper = setup(props);
    });
    
    it('should have the --disabled modifier', () => {
      expect(wrapper.find('.card.card--disabled').length).toBe(1);
    });
  });
});