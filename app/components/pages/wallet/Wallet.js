import React from 'react';
import PropTypes from 'prop-types';
import { I18n } from 'react-i18nify';
import { connect } from 'react-redux';
import { fetchCreditCards, addCreditCard, deleteCreditCard } from 'actions/creditCards';
import Card from 'components/common/card/Card';
import NewCardForm from 'components/common/forms/NewCard';
import './Wallet.scss';

export class Wallet extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      newCard: false
    };

    this.handleAddCardClick = this.handleAddCardClick.bind(this);
    this.handleCancelAddCardClick = this.handleCancelAddCardClick.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
  }

  componentDidMount() {
    this.props.fetchCreditCards();
  }

  handleAddCardClick() {
    this.setState({ newCard: true });
  }

  handleCancelAddCardClick() {
    this.setState({ newCard: false });
  }

  handleRemove() {
    // console.log('Remove: ', card);
  }

  get apiErrors() {
    const { isSaving, hasSavingErrors, savingErrors } = this.props;
    const errors = {};

    if (!isSaving && hasSavingErrors && savingErrors) {
      errors.api = I18n.t(`forms.errors.${savingErrors}`);
    }

    return errors;
  }

  renderActions() {
    const { newCard } = this.state;
    let actions;

    if (newCard) {
      actions = (
        <NewCardForm
          onCancel={this.handleCancelAddCardClick}
          onSubmit={this.props.addCreditCard}
          customErrors={this.apiErrors}
        />);
    } else {
      actions = <a className="btn btn--primary btn--large wallet-page__add-card" onClick={this.handleAddCardClick}>{ I18n.t('pages.wallet.addCard') }</a>;
    }

    return (
      <div className="wallet-page__actions">
        { actions }
      </div>
    );
  }

  render() {
    const { cards } = this.props;

    return (
      <div className="wallet-page">
        <h1 className="wallet-page__title">{ I18n.t('pages.wallet.title') }</h1>
        { cards && (
          <div className="wallet-page__cards">
            { cards.map((card, i) => (
              <div key={i} className="wallet-page__card">
                <Card {...card} onRemove={() => this.props.deleteCreditCard(card.id)} />
              </div>
            )) }
          </div>
        )}
        { this.renderActions() }
      </div>
    );
  }
}

Wallet.defaultProps = {
  isFetching: false,
  isSaving: false,
  hasSavingErrors: false,
  isDeleting: false,
  fetchCreditCards: () => {},
  addCreditCard: () => {},
  deleteCreditCard: () => {}
};

Wallet.propTypes = {
  isFetching: PropTypes.bool,
  isSaving: PropTypes.bool,
  hasSavingErrors: PropTypes.bool,
  savingErrors: PropTypes.string,
  isDeleting: PropTypes.bool,
  cards: PropTypes.array,
  fetchCreditCards: PropTypes.func,
  addCreditCard: PropTypes.func,
  deleteCreditCard: PropTypes.func
};

const mapStateToProps = (state) => {
  return {
    isFetching: state.fetchCreditCardsReducer.isFetching,
    isSaving: state.addCreditCardReducer.isFetching,
    hasSavingErrors: state.addCreditCardReducer.isError,
    savingErrors: state.addCreditCardReducer.message,
    isDeleting: state.deleteCreditCardReducer.isFetching,
    cards: state.fetchCreditCardsReducer.payload
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCreditCards: () => dispatch(fetchCreditCards()),
    addCreditCard: (form) => dispatch(addCreditCard(form)),
    deleteCreditCard: (id) => dispatch(deleteCreditCard(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Wallet);
