import React from 'react';
import { shallow } from 'enzyme';
import { Wallet } from './Wallet';

function setup(props) {
  return shallow(<Wallet {...props} />);
}

describe('Wallet', () => {
  describe('WHEN rendered by default', () => {
    let wrapper;
    
    beforeAll(() => {
      wrapper = setup();
    });
    
    it('should render a title and actions section', () => {
      expect(wrapper.find('.wallet-page__title').length).toBe(1);
      expect(wrapper.find('.wallet-page__actions').length).toBe(1);
    });
    
    it('should NOT render a cards section', () => {
      expect(wrapper.find('.wallet-page__cards').length).toBe(0);
    });
  });    
  
  describe('WHEN passing on card data', () => {
    let wrapper;
    const props = {
      cards: [
        {
          id: 0,
          number: '9554',
          expires: '04/2018',
        },
        {
          id: 1,
          number: '14265',
          expires: '04/2018',
        },
      ]
    };
    
    beforeAll(() => {
      wrapper = setup(props);
    });
    
    it('should render a cards section with two Card components', () => {
      expect(wrapper.find('.wallet-page__cards').length).toBe(1);
      expect(wrapper.find('.wallet-page__cards Card').length).toBe(2);
    });
  });    
  
});