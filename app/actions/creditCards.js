import * as types from './actionTypes';
import CreditCardService from 'services/creditCardServices';

const creditCardService = new CreditCardService();

function fetchCreditCardsRequest() {
  return {
    type: types.FETCH_CREDIT_CARDS_REQUEST
  };
}

function fetchCreditCardsSuccess(response) {
  return {
    type: types.FETCH_CREDIT_CARDS_SUCCESS,
    payload: response
  };
}

function fetchCreditCardsError(message) {
  return {
    type: types.FETCH_CREDIT_CARDS_ERROR,
    message
  };
}

function addCreditCardRequest() {
  return {
    type: types.ADD_CREDIT_CARD_REQUEST
  };
}

function addCreditCardsSuccess(response) {
  return {
    type: types.ADD_CREDIT_CARD_SUCCESS,
    payload: response
  };
}

function addCreditCardsError(message) {
  return {
    type: types.ADD_CREDIT_CARD_ERROR,
    message
  };
}

function deleteCreditCardRequest() {
  return {
    type: types.REMOVE_CREDIT_CARD_REQUEST
  };
}

function deleteCreditCardsSuccess(response) {
  return {
    type: types.REMOVE_CREDIT_CARD_SUCCESS,
    payload: response
  };
}

function deleteCreditCardsError(message) {
  return {
    type: types.REMOVE_CREDIT_CARD_ERROR,
    message
  };
}

// -- Action Creators --
export function fetchCreditCards() {
  return (dispatch) => {
    dispatch(fetchCreditCardsRequest());
    creditCardService
      .fetchCreditCards()
      .then(response => dispatch(fetchCreditCardsSuccess(response)))
      .catch(error => dispatch(fetchCreditCardsError(error)));
  };
}

export function addCreditCard(form) {
  return (dispatch) => {
    dispatch(addCreditCardRequest());
    creditCardService
      .addCreditCard(form)
      .then(response => {
        if (response.success) {
          dispatch(addCreditCardsSuccess(response));
          dispatch(fetchCreditCards());
        } else {
          dispatch(addCreditCardsError(response.message));
        }
      })
      .catch(error => dispatch(addCreditCardsError(error)));
  };
}

export function deleteCreditCard(id) {
  return (dispatch) => {
    dispatch(deleteCreditCardRequest());
    creditCardService
      .deleteCreditCard(id)
      .then(response => {
        dispatch(deleteCreditCardsSuccess(response));
        dispatch(fetchCreditCards());
      })
      .catch(error => dispatch(deleteCreditCardsError(error)));
  };
}
