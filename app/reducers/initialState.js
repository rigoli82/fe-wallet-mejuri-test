export default {
  creditCards: {
    fetch: {
      isFetching: false,
      isError: false,
      isSuccess: false,
      payload: [],
      message: null
    },
    add: {
      isFetching: false,
      isError: false,
      isSuccess: false,
      message: null
    },
    remove: {
      isFetching: false,
      isError: false,
      isSuccess: false,
      message: null
    }
  }
};
