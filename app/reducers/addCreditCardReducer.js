import initialState from './initialState';
import * as types from 'actions/actionTypes';

export default (state = initialState.creditCards.add, action) => {
  switch (action.type) {
    case types.ADD_CREDIT_CARD_REQUEST:
      return {
        ...state,
        isFetching: true,
        isError: false,
        isSuccess: false,
      };
    case types.ADD_CREDIT_CARD_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isSuccess: true,
      };
    case types.ADD_CREDIT_CARD_ERROR:
      return {
        ...state,
        isFetching: false,
        isError: true,
        message: action.message
      };
    default:
      return state;
  }
};
