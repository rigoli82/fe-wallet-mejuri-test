import initialState from './initialState';
import * as types from 'actions/actionTypes';

export default (state = initialState.creditCards.fetch, action) => {
  switch (action.type) {
    case types.FETCH_CREDIT_CARDS_REQUEST:
      return {
        ...state,
        isFetching: true,
        isSuccess: false,
        isError: false,
        payload: state.payload || initialState.creditCards.fetch.payload
      };
    case types.FETCH_CREDIT_CARDS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isSuccess: true,
        payload: action.payload
      };
    case types.FETCH_CREDIT_CARDS_ERROR:
      return {
        ...state,
        isFetching: false,
        isError: true,
        message: action.message
      };
    default:
      return state;
  }
};
