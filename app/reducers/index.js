import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';
import fetchCreditCardsReducer from './fetchCreditCardsReducer';
import addCreditCardReducer from './addCreditCardReducer';
import deleteCreditCardReducer from './deleteCreditCardReducer';

const rootReducer = combineReducers({
  fetchCreditCardsReducer,
  addCreditCardReducer,
  deleteCreditCardReducer,
  routing
});

export default rootReducer;
