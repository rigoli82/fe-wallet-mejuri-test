import httpHelper from 'helpers/httpHelper';
import { API_URL } from 'config/api';
import { encrypt } from 'helpers/encryptHelper';

export default class {
  fetchCreditCards() {
    return httpHelper(`${API_URL}/credit_cards.json`);
  }
  
  addCreditCard(form) {
    const data = {
      number: encrypt(form.number),
      cvv: encrypt(form.cvv),
      expires: encrypt(form.expires)
    };

    return httpHelper(`${API_URL}/credit_cards`, data, 'POST');
  }

  deleteCreditCard(id) {
    return httpHelper(`${API_URL}/credit_cards/${id}`, null, 'DELETE');
  }
}
