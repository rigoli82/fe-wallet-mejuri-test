export default (url, data, method = 'GET') => {
  let options = { method };
  
  if (data) {
    options = {
      ...options,
      body: JSON.stringify(data),
      cache: 'no-cache',
      headers: {
        'user-agent': 'Mozilla/4.0 MDN Example',
        'content-type': 'application/json'
      },
      mode: 'cors',
      redirect: 'follow',
      referrer: 'no-referrer'
    };
  }

  return fetch(url, options)
    .then(response => response && response.json())
    .catch(error => console.error('HTTP Fetching Error:', error)); // eslint-disable-line no-console
};
