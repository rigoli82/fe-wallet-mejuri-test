import { API_PUBLIC_KEY } from 'config/api';

export const encrypt = string => {
  const jse = new window.JSEncrypt();
  jse.setPublicKey(API_PUBLIC_KEY);
  return jse.encrypt(string);
};
