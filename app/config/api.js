export const API_URL = process.env.NODE_ENV === 'production'
  ? 'https://api-wallet-mejuri-test-mrigoli.c9users.io/api/v1' // Just in case
  : 'https://api-wallet-mejuri-test-mrigoli.c9users.io/api/v1';

export const API_PUBLIC_KEY = `-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDOjE+nXJIWeZkF0bzTp+Za/6BS
z4++FyrIY6fdv5xzVJA8mX7+SmVcTO7n2RyUViKQNn/RrPcZebkCWyQfjC5g9MHT
y4dLzMpba16LrwOHls6TWfidXEZ7fAABJkWsxbmZfHPpB/9iozlKKU+FVHplDTPh
IQ9bud1lTZ1QcQYlWQIDAQAB
-----END PUBLIC KEY-----`;
