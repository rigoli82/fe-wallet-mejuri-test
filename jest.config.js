module.exports = {
  testEnvironment: "jsdom",
  moduleNameMapper: {
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/tools/assetsTransformer.js",
    "\\.(css|less|scss)$": "<rootDir>/tools/assetsTransformer.js"
  },
  setupFiles: [
    "raf/polyfill",
    "<rootDir>/setupTests.js"
  ],
  testPathIgnorePatterns: [
    "/node_modules/"
  ],
  notify: true
};
